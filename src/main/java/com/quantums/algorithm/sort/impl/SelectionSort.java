package com.quantums.algorithm.sort.impl;

import com.quantums.algorithm.sort.AbstractSortAlgorithm;

/**
 * Created by user on 18/8/17.
 */
public class SelectionSort extends AbstractSortAlgorithm {

    public SelectionSort(){
        this.name = "选择排序";
    }

    @Override
    public int[] sort(int[] a) {
        for(int i=0;i<a.length;i++){
            int current = a[i];
            for(int j=i+1;j<a.length;j++){
                if(current>a[j]){
                    int temp = current;
                    current = a[j];
                    a[j] = temp;
                }
            }
            a[i] = current;
        }

        return a;
    }
}
