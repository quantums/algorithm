package com.quantums.algorithm.sort.impl;

import com.quantums.algorithm.sort.AbstractSortAlgorithm;

import java.util.Arrays;

/**
 * Created by user on 18/8/17.
 */
public class MergeSort extends AbstractSortAlgorithm {

    public MergeSort() {
        this.name = "归并排序";
    }

    @Override
    public int[] sort(int[] a) {

        return mergeSort(a);
    }


    private int[] mergeSort(int[] a) {
        if (a.length == 1) return a;

        if (a.length == 2) {
            if (a[0] > a[1]) {
                int tmp = a[0];
                a[0] = a[1];
                a[1] = tmp;
            }
            return a;
        }

        int mid = (0 + a.length) / 2;


        int[] bb = mergeSort(Arrays.copyOfRange(a, 0, mid));
        int[] cc = mergeSort(Arrays.copyOfRange(a, mid, a.length));

        return merge(bb,cc);
    }


    private int[] merge(int[] a, int[] b) {
        int m = 0, n = 0;
        int[] c = new int[a.length + b.length];

        for (int i = 0; i < c.length; i++) {
            if (m == a.length) {
                c[i] = b[n++];
            } else if (n == b.length) {
                c[i] = a[m++];
            } else {
                if (a[m] > b[n]) {
                    c[i] = b[n++];
                } else {
                    c[i] = a[m++];
                }
            }

        }
        return c;
    }
}
