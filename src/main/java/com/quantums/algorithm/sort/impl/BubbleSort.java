package com.quantums.algorithm.sort.impl;

import com.quantums.algorithm.sort.AbstractSortAlgorithm;

/**
 * Created by user on 18/8/17.
 */
public class BubbleSort extends AbstractSortAlgorithm {

    public BubbleSort() {
        this.name = "冒泡排序";
    }

    @Override
    public int[] sort(int[] a) {
        for(int i=0;i<a.length;i++){
            for(int j=0;j<a.length-i-1;j++){
                if(a[j]>a[j+1]){
                    int temp = a[j];
                    a[j]=a[j+1];
                    a[j+1]=temp;
                }
            }
        }
        return a;
    }
}
