package com.quantums.algorithm.sort.impl;

import com.quantums.algorithm.sort.AbstractSortAlgorithm;

/**
 * Created by user on 18/8/17.
 */
public class QuickSort extends AbstractSortAlgorithm {

    public QuickSort() {
        this.name = "快速排序";
    }

    @Override
    public int[] sort(int[] a) {
        quick(a, 0, a.length - 1);
        return a;
    }


    void quick(int[] a, int start, int end) {
        int current = a[start];
        /**终止条件一**/
        if(start==end) return;

        /**终止条件二**/
        if(start==end-1){
            if(a[start]>a[end]){
                int t = a[start];
                a[start] = a[end];
                a[end]=t;
            }
            return;
        }

        int i = start + 1, j = end;
        for (; i < j; ) {
            while (a[j] >= current && j>i) {
                j--;
            }

            while (a[i] <= current && i<j) {
                i++;
            }

            if (i < j) {
                int temp = a[i];
                a[i] = a[j];
                a[j] = temp;
            }
        }
        int t = a[i];
        a[i] = a[start];
        a[start] = t;

        /**递归完成剩下工作**/
        quick(a, start, i - 1);
        quick(a, i + 1, end);
    }
}
