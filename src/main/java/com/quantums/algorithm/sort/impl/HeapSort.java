package com.quantums.algorithm.sort.impl;

import com.quantums.algorithm.sort.AbstractSortAlgorithm;


/**
 * 堆排序
 */
public class HeapSort extends AbstractSortAlgorithm {

    public HeapSort() {
        this.name = "堆排序";
    }

    @Override
    public int[] sort(int[] a) {
        /**1. 构造最大堆: 这里可以优化,i=a.length/2开始**/
        for (int i = a.length - 1; i >= 0; i--) {
            sink(a, i, a.length - 1);
        }


        /**2. 弹出head, 并且和末尾的数字交换, 重构剩下n-1个元素的最大堆(sink)，然后依次弹出...**/
        int count = a.length - 1;
        while (count > 0) {
            swap(a, count, 0);
            sink(a,0,--count);
        }

        return a;
    }

    /**
     * 位置k的元素下沉，n是当前最大堆的长度
     * @param a
     * @param k
     * @param n
     */
    private void sink(int[] a, int k, int n) {
        /**注意这个堆是index=0开始的, 与BinaryHeap对象里面的不同**/
        while (2 * k + 2 <= n && (a[k] < a[2 * k + 1] || a[k] < a[2 * k + 2])) {
            int j = a[2 * k+1] >= a[2 * k + 2] ? (2 * k+1) : (2 * k + 2);
            swap(a, k, j);
            k = j;
        }
    }

    /**
     * 交换i和j位置的元素
     * @param a
     * @param i
     * @param j
     */
    private void swap(int[] a, int i, int j) {
        int tmp = a[i];
        a[i] = a[j];
        a[j] = tmp;
    }

}
