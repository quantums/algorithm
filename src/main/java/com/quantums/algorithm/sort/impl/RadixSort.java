package com.quantums.algorithm.sort.impl;

import com.quantums.algorithm.sort.AbstractSortAlgorithm;

/**
 * Created by user on 18/8/17.
 */
public class RadixSort extends AbstractSortAlgorithm {

    public RadixSort(){
        this.name = "基数排序";
    }

    @Override
    public int[] sort(int[] a) {
        int[] b = new int[10];
        for(int i=0;i<a.length;i++){
            b[a[i]]+=1;
        }

        int j=0;
        for(int i=0;i<b.length;i++){

            while (b[i]>0){
                a[j++]=i;
                b[i]-=1;
            }
        }

        return a;

    }
}
