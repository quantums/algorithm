package com.quantums.algorithm.sort;

import com.quantums.algorithm.sort.impl.*;

import java.util.*;

/**
 * Created by user on 18/8/17.
 */
public class TestSort {

    public static void main(String []ar){
        int[] a = {5,3,5,2,8,9,4,0,12,21,60,1,30};
        List<AbstractSortAlgorithm> list = new ArrayList<>();
        list.add(new RadixSort());
        list.add(new HeapSort());
        list.add(new BubbleSort());
        list.add(new SelectionSort());
        list.add(new QuickSort());
        list.add(new MergeSort());
        runSort(list,a);
}


    private static void runSort(List<AbstractSortAlgorithm> sorts, int[] a){
        for(AbstractSortAlgorithm s:sorts){
            System.out.printf("%-5s : %s\n",s.getName(),Arrays.toString(s.sort(a.clone())));
        }
    }


    ThreadLocal t;
}
