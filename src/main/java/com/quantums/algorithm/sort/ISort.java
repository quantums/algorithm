package com.quantums.algorithm.sort;

/**
 * Created by user on 18/8/17.
 */
public interface ISort {
    int[] sort(int[] a);
}
