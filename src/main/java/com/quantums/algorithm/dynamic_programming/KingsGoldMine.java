package com.quantums.algorithm.dynamic_programming;


/**
 * https://mp.weixin.qq.com/s/3h9iqU4rdH3EIy5m6AzXsg
 * 国王的金矿:
 * 有一个国家发现了5座金矿，每座金矿的黄金储量不同，需要参与挖掘的工人数也不同。
 * 参与挖矿工人的总数是10人。每座金矿要么全挖，要么不挖，不能派出一半人挖取一半金矿。
 * 要想得到尽可能多的黄金，应该选择挖取哪几座金矿？
 * <p>
 * 5座金矿的金子储量/所需人力:
 * 400金/5人、500金/5人、200金/3人、30金/4人、350金/3人
 */
public class KingsGoldMine {

    /**
     * 5座金矿按上述编号0-5
     */
//    private int[] gold = {500, 200, 300, 350, 400}; //黄金储量
    private int[] gold = {400, 500, 200, 300, 350}; //黄金储量
//    private int[] labor = {5, 3, 4, 3, 5}; //所需人力
    private int[] labor = {5, 5, 3, 4, 3}; //所需人力

    /**
     * 人力1~10的时候的收获
     */
    private int[][] harvest = new int[5][11];

    private int max(int a, int b) {
        return a > b ? a : b;
    }


    /**
     * 1. 动态规划自底向上推倒
     * 时间复杂度为: 金矿数目 * 总的人数
     * 空间复杂度为: 金矿数目 * 总的人数
     *
     */
    private void compute() {
        printMatrix(harvest);

        System.out.println("\n*************************************************************************\n");
        for (int i = 0; i < 5; i++) {
            for (int j = 1; j < 11; j++) {
                if (i == 0) {
                    if (j < labor[i])
                        harvest[i][j] = 0;
                    else
                        harvest[i][j] = gold[i];
                } else {
                    if (j < labor[i])
                        harvest[i][j] = max(harvest[i - 1][j], 0);
                    else
                        harvest[i][j] = max(harvest[i - 1][j], gold[i] + harvest[i - 1][j - labor[i]]);
                }
            }
        }
        printMatrix(harvest);
    }


    public void printMatrix(int[][] a) {
        for (int i = 0; i < a.length; i++) {
            for (int j = 1; j < a[i].length; j++) {
                System.out.printf("%-4d\t", a[i][j]);
            }
            System.out.println("");
        }
    }

    public static void main(String[] ar) {
        KingsGoldMine gm = new KingsGoldMine();
        gm.compute();
    }
}
