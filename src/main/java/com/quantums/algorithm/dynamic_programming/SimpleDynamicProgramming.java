package com.quantums.algorithm.dynamic_programming;


/**
 * https://mp.weixin.qq.com/s/3h9iqU4rdH3EIy5m6AzXsg
 * <p>
 * 爬楼梯的简单动态规划问题:
 * 有一座高度是10级台阶的楼梯，从下往上走，每跨一步只能向上1级或者2级台阶。要求用程序来求出一共有多少种走法。
 */
public class SimpleDynamicProgramming {


    public static void main(String[] ar) {

        for (int i = 1; i < 11; i++) {
            System.out.printf("%-2d 步台阶 : %-2d\t", i, new RecursiveDP().countClimbs(i));
            System.out.printf("%-2d\t",new CachedDP(i).countClimbs(i));
            System.out.printf("%-2d\n",new RealDP().countClimbs(i));
        }
    }
}

interface IClimb {
    int countClimbs(int n);
}

/**
 * 1. 最直观的递归方式来计算
 */
class RecursiveDP implements IClimb {
    /**
     * f(n) = f(n-1)+f(n-2)
     * 这种方法的问题在于会重复计算很多中间结果
     *
     * @return
     */
    @Override
    public int countClimbs(int n) {
        if (n < 1) return 0;

        if (n == 1) return 1;

        if (n == 2) return 2;

        return countClimbs(n - 1) + countClimbs(n - 2);
    }

}


/**
 * 2. 备忘录算法: 缓存历史结果, 从而不必多次计算
 */
class CachedDP implements IClimb {
    private int[] cache;

    public CachedDP(int n) {
        cache = new int[n + 1];
        for (int i = 0; i < cache.length; i++) cache[i] = 0;
    }

    @Override
    public int countClimbs(int n) {
        if (n < 1) return 0;

        if (n == 1) return 1;

        if (n == 2) return 2;

        if (cache[n - 1] == 0) cache[n - 1] = countClimbs(n - 1);
        if (cache[n - 2] == 0) cache[n - 2] = countClimbs(n - 2);
        cache[n] = cache[n - 1] + cache[n - 2];
        return cache[n];
    }
}

/**
 * 3. 自底向上的推导
 */
class RealDP implements IClimb {

    @Override
    public int countClimbs(int n) {
        if (n == 1) return 1;

        if (n == 2) return 2;

        int a = 1, b = 2;
        for (int i = 3; i <= n; i++) {
            int tmp = b;
            b = a + b;
            a = tmp;
        }

        return b;
    }
}