package com.quantums.algorithm.search;

public class BinarySearch implements ISearch {
    @Override
    public int search(int[] a, int target) {
        if (a == null) return -1;

        return search(a, target, 0, a.length - 1);


    }

    private int search(int[] a, int target, int start, int end) {
        if (start > end) return -1;

        int mid = (start + end) / 2;

        if (a[mid] == target) return mid;

        else if (a[mid] > target) return search(a, target, start, mid - 1);

        else return search(a, target, mid + 1, end);
    }


    public static void main(String[] ar) {

        int[] a = {1, 3, 5, 6, 8, 9, 12, 15};
        ISearch binary = new BinarySearch();
        System.out.println(binary.search(a, 15));
        System.out.println(binary.search(a, 1));
        System.out.println(binary.search(a, 6));
        System.out.println(binary.search(a, 19));
        System.out.println(binary.search(a, 11));
    }
}
