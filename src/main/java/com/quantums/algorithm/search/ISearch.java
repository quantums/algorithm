package com.quantums.algorithm.search;

/**
 * Created by user on 18/8/17.
 */
public interface ISearch {

    int search(int[] a, int target);
}
