package com.quantums.quiz.top_k;

import java.util.Arrays;

/**
 * 首先, 通过排序很容易获得结果,复杂度o(n*log(n)) 这种方法略过...
 * 其次, 只需要部分排序, 这样复杂度只需要o(k*n)
 */
public class TopkByPartSort implements TopK {
    @Override
    public int[] topK(int[] a, int k) {

        for (int i = 0; i < k; i++) {
            int min = i;
            for (int j = i; j < a.length; j++) {
                if (a[j] > a[min]) {
                    min = j;
                }
            }

            int tmp = a[i];
            a[i] = a[min];
            a[min] = tmp;
        }

        return Arrays.copyOfRange(a, 0, k);
    }
}
