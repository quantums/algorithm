package com.quantums.quiz.top_k;

public interface TopK {

    int[] topK(int[] a, int k);
}
