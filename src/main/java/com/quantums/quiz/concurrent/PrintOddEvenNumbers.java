package com.quantums.quiz.concurrent;


import java.util.concurrent.TimeUnit;

/**
 * 两个线程交替打印奇数、偶数
 */
public class PrintOddEvenNumbers {
    private static volatile boolean isOdd = true;

    private static int count = 1;

    public static void main(String[] ar) {
        one();
        two();
    }

    /*******************************第一种: 比较好**************************/
    private static void one() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                while (true) {
                    if (isOdd) {
                        System.out.println("奇数 ===== " + count++);
                        isOdd = false;
                    }

                    try {
                        TimeUnit.MICROSECONDS.sleep(100);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }).start();

        new Thread(new Runnable() {
            @Override
            public void run() {
                while (true) {
                    if (!isOdd) {
                        System.out.println("偶数 = " + count++);
                        isOdd = true;
                    }

                    try {
                        TimeUnit.MICROSECONDS.sleep(100);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }).start();

    }

    /*******************************第二种: 效率更低一点，方法更复杂**************************/
    private static void two() {
        final PrintOddEvenNumbers print = new PrintOddEvenNumbers();
        new Thread(new Runnable() {
            @Override
            public void run() {
                print.printOdd();
            }
        }).start();

        new Thread(new Runnable() {
            @Override
            public void run() {
                print.printEven();
            }
        }).start();

    }


    private synchronized void printOdd() {
        for (; ; ) {
            System.out.println("奇数 ==== " + count++);
            this.notify();
            try {
                this.wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }


    private synchronized void printEven() {
        for (; ; ) {
            System.out.println("偶数 = " + count++);
            this.notify();
            try {
                this.wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
