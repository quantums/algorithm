package com.quantums.quiz.math;

/**
 * 求最大公约数
 */
public class GreatestCommonDivisor {

    /**
     * 方法一 : a和b的最大公约数 = a-b和b的最大公约数 (a>b)
     *
     * @param a
     * @param b
     * @return
     */
    public static int gcd_1(int a, int b) {
        if (a == b) return a;

        return a > b ? gcd_1(a - b, b) : gcd_1(a, b - a);
    }


    /**
     * 方法二 : a、b的最大公约数 = a%b、b的最大公约数
     *
     * @param a
     * @param b
     * @return
     */
    public static int gcd_2(int a, int b) {
        if (a == b) return a;

        int min = a < b ? a : b;
        int max = a > b ? a : b;

        if (min == 0) return max;

        return gcd_2(max % min, min);
    }


    /**
     * 方法三 : 综合方法一和方法二进行优化
     *
     * @param a
     * @param b
     * @return
     */
    public static int gcd_3(int a, int b) {

        return 0;
    }


    public static void main(String[] ar) {
        System.out.println(gcd_1(20, 9));
        System.out.println(gcd_1(20, 24));

        System.out.println(gcd_2(20, 9));
        System.out.println(gcd_2(20, 24));
    }
}
