package com.quantums.quiz.array;

import com.quantums.algorithm.sort.ISort;
import com.quantums.algorithm.sort.impl.BubbleSort;

import java.util.Arrays;

/**
 * 找出数组中两个数字a,b，使得a+b最接近指定数字v
 */
public class TwoSumCloseToValue {

    public void getClosed(int[] a, int v) {
        ISort quick = new BubbleSort();
        a = quick.sort(a);
        System.out.println(Arrays.toString(a));


    }

    public static void main(String[] ar) {
        int[] a = {2, 1, 3, 5, 10, 32, 10, 8,34,36,44,55};
        new TwoSumCloseToValue().getClosed(a, 33);
    }
}
