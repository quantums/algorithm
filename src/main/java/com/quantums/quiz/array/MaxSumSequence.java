package com.quantums.quiz.array;


/***
 *
 * 求数组的子序列最大和
 */
public class MaxSumSequence {

    public static void main(String[] ar) {
        int[] a = {-1, 3, 5, -4, -1, 7, -10, 12, 3, 11, 9, 0, -5, 0, -5, 3};

        System.out.println(maxSub(a));
        System.out.println(maxSubImprove(a));
    }


    /***
     * 从前到后计算所有子序列的和
     *
     * @param a
     * @return
     */
    public static int maxSub(int[] a) {
        int maxSum = Integer.MIN_VALUE;

        for (int i = 0; i < a.length; i++) {
            for (int j = i; j < a.length; j++) {
                int sum = 0;
                for (int k = i; k <= j; k++) {
                    sum += a[k];
                }
                if (sum > maxSum)
                    maxSum = sum;
            }
        }

        return maxSum;
    }



    public static int maxSubImprove(int[] a) {
        int maxSum = Integer.MIN_VALUE;

        for (int i = 0; i < a.length; i++) {
             int sum = 0;
            for (int j = i; j < a.length; j++) {
                sum += a[j];
                if (sum > maxSum)
                    maxSum = sum;
            }
        }

        return maxSum;
    }

}
