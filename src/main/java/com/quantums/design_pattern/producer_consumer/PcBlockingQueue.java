package com.quantums.design_pattern.producer_consumer;


import java.util.Random;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.TimeUnit;

/**
 * 生产者消费者模式 : 阻塞队列实现
 */
public class PcBlockingQueue {
    private BlockingQueue queue = new ArrayBlockingQueue<Integer>(10);
    private Random random = new Random(100);
    private volatile boolean stop = false;


    public void consume() {
        while (!stop || !queue.isEmpty()) {
            try {
                TimeUnit.MILLISECONDS.sleep(random.nextInt(100));
                Integer t = (Integer) queue.poll();
                System.out.println("消费-->>> " + t);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public void produce() {
        int i = 0;
        while (i < 10) {
            System.out.println("生产:" + i++);
            try {
                TimeUnit.MILLISECONDS.sleep(random.nextInt(100));
                queue.put(i);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        stop = true;
    }

    public static void main(String[] ar) {
        PcBlockingQueue pc = new PcBlockingQueue();
        new Thread(() -> pc.produce()).start();
        new Thread(() -> pc.produce()).start();
        new Thread(() -> pc.consume()).start();
    }
}


