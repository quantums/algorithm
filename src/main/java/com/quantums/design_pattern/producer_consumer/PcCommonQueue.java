package com.quantums.design_pattern.producer_consumer;


import java.util.LinkedList;
import java.util.Queue;
import java.util.Random;
import java.util.concurrent.TimeUnit;

/**
 * 普通队列实现
 */
public class PcCommonQueue {
    private static Queue<Integer> queue = new LinkedList<>();
    private static volatile boolean stop = false;
    private static Random random = new Random(100);

    static class Producer implements Runnable {

        @Override
        public void run() {
            int i = 0;

            while (i < 100) {
                try {

                    TimeUnit.MILLISECONDS.sleep(random.nextInt(100));

                    if (queue.size() == 10) {

                        /**调用wait()必须在持有锁的情况下, 否则抛出IllegalMonitorStateException**/
                        synchronized (queue) {
                            queue.wait();
                        }
                    } else {
                        queue.add(i);
                        System.out.println("生产:" + i++);
                        if (queue.size() == 5) {

                            /**调用notify()必须在持有锁的情况下, 否则抛出IllegalMonitorStateException**/
                            synchronized (queue) {
                                queue.notifyAll();
                            }
                        }
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

            }

            stop = true;
        }
    }


    static class Consumer implements Runnable {

        @Override
        public void run() {
            while (!stop || !queue.isEmpty()) {
                try {
                    TimeUnit.MILLISECONDS.sleep(random.nextInt(100));

                    if (queue.isEmpty()) {
                        /**调用wait()必须在持有锁的情况下, 否则抛出IllegalMonitorStateException**/
                        synchronized (queue) {
                            queue.wait();
                        }
                    } else {
                        System.out.println("消费-->>>" + queue.remove());
                        if (queue.size() == 5) {

                            /**调用notify()必须在持有锁的情况下, 否则抛出IllegalMonitorStateException**/
                            synchronized (queue) {
                                queue.notifyAll();
                            }
                        }
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }

    }


    public static void main(String[] ar) {
        new Thread(new Producer()).start();
        new Thread(new Consumer()).start();
    }
}



