package com.quantums.design_pattern.singleton;

/**
 * 延迟初始化: 针对DoubleCheckedLock初次多线程竞争时候的效率问题, 利用JDK类加载时的锁机制
 */
public class LazyInitialization {


    private static class Holder {
        public final static LazyInitialization instance = new LazyInitialization();
    }

    private LazyInitialization() {

    }

    public static LazyInitialization getInstance() {
        return Holder.instance;
    }


}
