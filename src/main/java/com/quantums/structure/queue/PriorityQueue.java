package com.quantums.structure.queue;


import java.util.Arrays;

/***
 * 二叉堆: 利用数组来实现完全二叉树
 * 忽略index=0的数组元素，从1开始。
 * index=i的元素, 它的左右子节点分别为2i和2i+1, 它的父亲节点为i/2
 *
 */
public class PriorityQueue<T extends Comparable> implements IHeap<T> {

    private Comparable[] table;

    private final static int DEFAULT_CAPACITY = 16; //默认capacity

    private int count = 0; //元素个数

    public PriorityQueue() {
        this(DEFAULT_CAPACITY);
    }

    public PriorityQueue(int cap) {
        this.table = new Comparable[cap + 1];
        this.count = 0;
    }

    /**
     * Build heap from arrays
     **/
    public PriorityQueue(T[] list) {
        this.table = new Comparable[list.length + 1];
        for (T t : list) {
            insert(t);
        }
    }


    @Override
    public T max() {
        if (table == null || count == 0) return null;
        return (T) table[1];
    }

    @Override
    public void insert(T t) {
        table[++count] = t;
        swim();
    }

    /**
     * 插入元素之后上浮最新节点
     */
    private void swim() {
        int num = count;
        while (num > 1 && ((T) table[num]).compareTo((T) table[num / 2]) > 0) {
            swap(num, num / 2);
            num = num / 2;
        }
    }

    /**
     * 交换i和j节点的元素
     */
    private void swap(int i, int j) {
        T tmp = (T) table[i];
        table[i] = table[j];
        table[j] = tmp;
    }


    /**
     * 弹出头节点, 接着把最末尾的节点作为头节点, 然后进行下沉操作, 这样能够保证剩下的节点依然组成了一棵完全二叉树
     */
    @Override
    public T delMax() {
        if (table == null || count == 0) return null;
        T result = (T) table[1];
        table[0] = table[count--]; //最末尾的节点作为头节点, 然后进行下沉操作, 这样能够保证剩下的节点依然组成了一棵完全二叉树
        sink();
        return result;
    }


    /**
     * 头节点因为比其子节点小而下沉
     */
    private void sink() {
        int num = 0;
        while (num <= count / 2 && (table[num].compareTo(table[2 * num]) < 0 || table[num].compareTo(table[2 * num + 1]) < 0)) {
            /**保证弹出的是左右节点中较大的那个**/
            int j = table[2 * num].compareTo(table[2 * num + 1]) > 0 ? (2 * num) : (2 * num + 1);
            swap(num, j);
            num = j;
        }
    }


    @Override
    public boolean isEmpty() {
        return count == 0;
    }

    @Override
    public int size() {
        return count;
    }

    @Override
    public String toString() {
            return Arrays.toString(Arrays.copyOfRange(table, 1, count+1));
    }
}
