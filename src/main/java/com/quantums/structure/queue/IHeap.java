package com.quantums.structure.queue;

/***
 *
 * 二叉堆
 *
 */
public interface IHeap<T extends Comparable> {

    T max();

    void insert(T t);

    T delMax();

    boolean isEmpty();

    int size();
}
