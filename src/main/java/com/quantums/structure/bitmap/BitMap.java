package com.quantums.structure.bitmap;

/**
 * Copyright (c) 2015 XiaoMi Inc. All Rights Reserved.
 * Authors: zhangjie <zhangjie23@xiaomi.com>
 */
public class BitMap {
    private char[] chars;//Java中char站2个字节、16位
    private int nBits;

    private final static int DEFAULT_LENGTH = 1024;

    public BitMap(int n) {
        this.nBits = n;
        chars = new char[1024 / 16 + 1];
    }


    public void setBit(int k) {
        if (k > nBits) return;
        int bytes = k / 16;
        int bits = k % 16;
        chars[bytes] |= (1 << bits);//1左移bits位
    }

    public void unsetBit(int k) {
        int bytes = k / 16;
        int bits = k % 16;
        chars[bytes] &= ~(1 << bits);//1左移bits位
    }

    public boolean getBit(int k) {
        int bytes = k / 16;
        int bits = k % 16;

        return (chars[bytes] & (1 << bits)) != 0;
    }


    public static void main(String[] ar) {
        BitMap bitMap = new BitMap(1024);
        bitMap.setBit(10);
        bitMap.setBit(99);
        System.out.print("10 = " + bitMap.getBit(10));
        System.out.println("\t99 = " + bitMap.getBit(99));

        bitMap.unsetBit(10);
        System.out.print("10 = " + bitMap.getBit(10));
        System.out.println("\t99 = " + bitMap.getBit(99));

        bitMap.setBit(10);
        System.out.print("10 = " + bitMap.getBit(10));
        System.out.println("\t99 = " + bitMap.getBit(99));
    }
}
