package com.quantums.structure.bloom_filter;


import java.util.BitSet;


/**
 * 布隆过滤器
 *
 */
public class BloomFilter {
    private final static int DEFAULT_CAP = 1 << 10; //BloomFilter的默认数组长度
    private BitSet bitSet = new BitSet(DEFAULT_CAP);
    private final static int[] seeds = {7, 11, 13, 17, 19, 23, 29, 31, 37, 53};
    private static SimpleHashAlgorithm[] hashs = new SimpleHashAlgorithm[seeds.length];

    private static void buildHashAlogrithms() {
        for (int i = 0; i < seeds.length; i++) {
            hashs[i] = new SimpleHashAlgorithm(DEFAULT_CAP, seeds[i]);
        }
    }

    public BloomFilter() {
        buildHashAlogrithms();
    }

    public void add(String val) {
        if (val == null) return;

        for (SimpleHashAlgorithm al : hashs) {
            bitSet.set(al.hash(val), true);
        }
    }

    public boolean contains(String str) {
        if (str == null) return false;

        for (SimpleHashAlgorithm al : hashs) {
            if (!bitSet.get(al.hash(str))) return false;
        }
        return true;
    }


    public static void main(String []ar){
        String[] origins = {"JAVA", "Linux","Go","C","Python","Shell"};
        BloomFilter bloomFilter = new BloomFilter();
        for(String s:origins){
            bloomFilter.add(s);
        }

        String[] tests = {"JAVA", "Linux","Go","C","Python","Shell","China","USA","Germany","UK","Keroa"};
        for (String s:tests){
            System.out.println(s+" "+bloomFilter.contains(s));
        }


    }

}


class SimpleHashAlgorithm {
    private int cap;
    private int seed;

    public SimpleHashAlgorithm(int c, int s) {
        this.cap = c;
        this.seed = s;
    }


    public int hash(String str) {
        int result = seed;
        for (int i = 0; i < str.length(); i++) {
            result = seed * result + str.charAt(i);
        }

        return (cap - 1) & result;
    }

}