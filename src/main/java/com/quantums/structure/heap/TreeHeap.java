package com.quantums.structure.heap;

/**
 * 基于二叉树实现堆
 */
public class TreeHeap extends AbstractHeap {

    TreeHeap() {
        this(DEFAULT_CAP);
    }

    TreeHeap(int s) {
        this.size = s;
    }

    @Override
    public void insert(Comparable comparable) {

    }

    @Override
    public Comparable max() {
        return null;
    }

    @Override
    public Comparable deleteMax() {
        return null;
    }

    @Override
    public boolean isEmpty() {
        return false;
    }

    @Override
    public int size() {
        return 0;
    }
}
