package com.quantums.structure.heap;

/**
 * 数据结构: 堆
 */
public interface IHeap<T extends Comparable>{

    void insert(T t);

    T max();

    T deleteMax();

    boolean isEmpty();

    int size();


}
