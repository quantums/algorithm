package com.quantums.structure.heap;

/**
 * 基于数组实现最小堆
 */
public class ArrayHeap<T extends Comparable> extends AbstractHeap {
    private T[] table;

    public ArrayHeap() {
        this(DEFAULT_CAP + 1);
    }

    public ArrayHeap(int cap) {
        this.capacity = cap + 1;
        table = (T[]) (new Object[capacity]);
    }

    @Override
    public void insert(Comparable comparable) {
        if (size < capacity - 1) {
            table[++size] = (T) comparable;
            swim();
        } else {
            if (table[1].compareTo(comparable) < 0) {
                table[1] = (T) comparable;
                sink();
            }
        }
    }

    private void swim() {
        int current = size;
        while (current >= 2 && table[current].compareTo(table[current / 2]) < 0) {
            T tmp = table[current / 2];
            table[current / 2] = table[current];
            table[current] = tmp;
        }
    }

    private void sink() {
        int current = 1;

        while (current <= size / 2){
        }
    }

    @Override
    public Comparable max() {
        if (table == null) return null;

        return table[1];
    }

    @Override
    public Comparable deleteMax() {
        return null;
    }

    @Override
    public boolean isEmpty() {
        return false;
    }

    @Override
    public int size() {
        return 0;
    }
}
