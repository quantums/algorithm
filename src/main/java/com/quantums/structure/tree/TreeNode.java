package com.quantums.structure.tree;

public class TreeNode<T> {

    T value;

    TreeNode left;

    TreeNode right;

    public TreeNode(T v, TreeNode l, TreeNode r) {
        this.value = v;
        this.left = l;
        this.right = r;
    }

    public TreeNode() {
        this(null, null, null);
    }

    public TreeNode(T t){
        this(t,null,null);
    }
}
