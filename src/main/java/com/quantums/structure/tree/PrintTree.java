package com.quantums.structure.tree;

import java.util.LinkedList;
import java.util.Queue;

/***
 *
 * 打印二叉树
 *
 */
public class PrintTree {


    public void printDepthFirst(TreeNode root) {
        if (root == null) return;

        System.out.printf("%d\t", root.value);

        if (root.left != null) printDepthFirst(root.left);
        if (root.right != null) printDepthFirst(root.right);
    }

    public int printWidthFirst(TreeNode root) {
        if (root == null) return -1;

        Queue<TreeNode> queue = new LinkedList<>();

        int level = 0;
        queue.add(root);
        while (!queue.isEmpty()) {
            System.out.print("LEVEL_" + (level++) + " : ");
            int size = queue.size();
            for (int i = 0; i < size; i++) {
                TreeNode node = queue.remove();
                System.out.printf("%d\t", node.value);
                if (node.left != null) queue.add(node.left);
                if (node.right != null) queue.add(node.right);
            }
            System.out.println("");
        }
        return 1;
    }


    public void printNodeAtLevel(TreeNode root, int level) {
        if (root == null) return;

        if (level == 1) {
            System.out.print(root.value + "\t");
        } else {
            printNodeAtLevel(root.left, level - 1);
            printNodeAtLevel(root.right, level - 1);
        }
    }


    public static void main(String[] ar) {
        TreeNode<Integer> root = new TreeNode(0);
        TreeNode<Integer> l = new TreeNode(1);
        TreeNode<Integer> r = new TreeNode(2);
        root.left = l;
        root.right = r;

        TreeNode<Integer> ll = new TreeNode(3);
        TreeNode<Integer> lr = new TreeNode(4);
        l.left = ll;
        l.right = lr;

        TreeNode<Integer> rl = new TreeNode(5);
        TreeNode<Integer> rr = new TreeNode(6);
        r.left = rl;
        r.right = rr;

        PrintTree print = new PrintTree();

        print.printDepthFirst(root);
        System.out.println();

        print.printWidthFirst(root);
        print.printNodeAtLevel(root, 3);
    }
}
