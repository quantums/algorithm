package com.quantums.structure.tree;

/**
 * 判断一棵树是否平衡二叉树
 */
public class CheckBalance {


    /***
     * 计算树的高度
     * @param root
     * @return
     */
    public static int height(TreeNode root) {
        if (root == null) return 0;

        int l = height(root.left);
        int r = height(root.right);
        return 1 + (l > r ? l : r);
    }


    /***
     * 判断一棵树是否平衡
     * @param root
     * @return
     */
    public static  boolean isBalance(TreeNode root) {
        if (root == null) return true;

        if (isBalance(root.left) && isBalance(root.right)) {
            int l = height(root.left);
            int r = height(root.right);
            if (l - r > -2 && l - r < 2) return true;
        }

        return false;
    }


    public static void main(String []ar){
        TreeNode<Integer> root = new TreeNode<>(1);
        TreeNode<Integer> left = new TreeNode<>(2);
        TreeNode<Integer> right = new TreeNode<>(3);
        TreeNode<Integer> ll = new TreeNode<>(4);
        TreeNode<Integer> lr = new TreeNode<>(6);
        TreeNode<Integer> rl = new TreeNode<>(5);
        TreeNode<Integer> rr = new TreeNode<>(7);

        root.left = left;
        root.right = right;

        left.left = ll;
        left.right = lr;


        right.left = rl;
        right.right = rr;

        System.out.println(height(root));
        System.out.println(height(right));
        System.out.println(height(rl));
        System.out.println(height(rl.left));

        System.out.println(isBalance(root));

        rl.left = new TreeNode();
        System.out.println(isBalance(root));

        rl.left.left = new TreeNode();
        System.out.println(isBalance(root));
    }
}
