package com.quantums.structure.map;

import java.util.HashMap;
import java.util.Hashtable;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by user on 18/8/18.
 */
public class TestMap {

    public static void main(String[] ar) {
        testNullable();

    }

    static void testNullable() {
        HashMap hashMap = new HashMap<String, String>();
        hashMap.put(null, "Jay");
        hashMap.put("Js", null);


        /**HashTable和ConcurrentHashMap的key, value都不能为null**/
        Hashtable hashtable = new Hashtable<String, String>();
        hashtable.put(null, "Jay");
        hashtable.put("J", null);

        ConcurrentHashMap concurrentHashMap = new ConcurrentHashMap<String, String>();
        concurrentHashMap.put(null, "Jay");
        concurrentHashMap.put("J", null);


        HashMap m;

    }

}
