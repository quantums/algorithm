package com.quantums.structure.list;


/***
 * 链表反转
 */
public class LinkedListInversion {

    /***
     * 顺序遍历
     */
    private static LinkedNode inverse(LinkedNode n) {
        if (n == null) return null;

        LinkedNode neu = null;
        LinkedNode head = n;

        while (head != null) {
            LinkedNode current = head;
            head = head.next;
            current.next = neu;
            neu = current;
        }
        return neu;
    }

    /**
     * 递归实现
     */
    private static LinkedNode inverseByRecursion(LinkedNode n) {

        if (n == null || n.next == null) return n;

        LinkedNode head = n; //先切掉头节点
        LinkedNode second = n.next; //第二个节点现在变成了rest的最后一个节点
        head.next = null;

        LinkedNode rest = inverseByRecursion(second); //翻转第二个之后的链表
        second.next = head; //

        return rest;
    }

    public static void main(String[] ar) {
        LinkedNode<Integer> a = new LinkedNode(1, null);
        LinkedNode<Integer> b = new LinkedNode(2, null);
        LinkedNode<Integer> c = new LinkedNode(3, null);
        LinkedNode<Integer> d = new LinkedNode(4, null);
        LinkedNode<Integer> e = new LinkedNode(5, null);
        a.next = b;
        b.next = c;
        c.next = d;
        d.next = e;
        System.out.println(a.toListString());
        System.out.println(LinkedListInversion.inverse(a).toListString());

        System.out.println(e.toListString());
        System.out.println(LinkedListInversion.inverseByRecursion(e).toListString());
    }
}
