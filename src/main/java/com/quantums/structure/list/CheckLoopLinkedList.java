package com.quantums.structure.list;

/**
 * 判断链表是否有环
 * https://mp.weixin.qq.com/s/3a-Y-EMvmxJwEBEDrep-yg
 * <p>
 * 方法一: HashSet保存遍历过的节点，然后比较新节点与已经遍历节点，若有相同节点，说明有环。
 * 方法二: 快慢指针法
 */
public class CheckLoopLinkedList {


    /**
     * 1. 快慢指针法
     *
     * @param head
     * @return
     */
    public static boolean isLooped(LinkedNode head) {
        if (head == null) return false;

        LinkedNode fast = head.next.next, slow = head.next;

        while (fast != null && slow != null) {
            if (fast == slow) return true;

            /**快指针每次走两步**/
            fast = fast.next.next;
            /**慢指针每次走一步**/
            slow = slow.next;
        }

        return false;
    }

    /**
     * 2. 求入环节点
     * fast和slow第一次相遇之后, fast回到头节点, 并且速度降到1,
     * 然后fast和slow再都以速度1开始遍历, 二者再次相遇的节点即为入环节点.
     *
     * @return
     */
    public static LinkedNode crossNode(LinkedNode head) {
        if (head == null) return null;

        LinkedNode fast = head.next.next, slow = head.next;

        while (fast != null && slow != null) {
            if (fast == slow) break;

            /**快指针每次走两步**/
            fast = fast.next.next;
            /**慢指针每次走一步**/
            slow = slow.next;
        }


        /**fast回到head，并且速度降低为1**/
        fast = head;
        while (fast != null && slow != null) {
            if (fast == slow) return fast;

            /****/
            fast = fast.next;
            slow = slow.next;
        }

        return null;
    }


    /**
     * 3. 求环的长度
     *
     * @param head
     * @return
     */
    public static int loopLength(LinkedNode head) {

        if (head == null) return 0;

        LinkedNode fast = head.next.next, slow = head.next;


        int meetTimes = 0, count = 0;
        while (fast != null && slow != null) {
            if (meetTimes == 1) count++;

            /**
             *两个第一次相遇, 从同一个位置开始走。
             *  当它们再次相遇: 这时候fast比slow多走了一圈；
             *  slow走了count, fast走了2*count
             *
             *  所以环的长度为count
             */
            if (fast == slow) {
                meetTimes += 1;

                /**第二次相遇, 退出**/
                if (meetTimes == 2) break;
            }

            /**快指针每次走两步**/
            fast = fast.next.next;
            /**慢指针每次走一步**/
            slow = slow.next;
        }

        return count;
    }


    /**
     * 测试
     *
     * @param ar
     */
    public static void main(String[] ar) {
        LinkedNode ten = new LinkedNode(10);
        LinkedNode nine = new LinkedNode(9, ten);
        LinkedNode eight = new LinkedNode(8, nine);
        LinkedNode seven = new LinkedNode(7, eight);
        LinkedNode six = new LinkedNode(6, seven);
        LinkedNode five = new LinkedNode(5, six);
        LinkedNode four = new LinkedNode(4, five);
        LinkedNode three = new LinkedNode(3, four);
        LinkedNode two = new LinkedNode(2, three);
        LinkedNode one = new LinkedNode(1, two);

        System.out.println(one.toListString());
        System.out.println(isLooped(one));

        /**three结环**/
        ten.next = three;
        System.out.printf("ten.next=%2d, 有环?%b\t", 3, isLooped(one));
        System.out.printf("入环节点=%s\t", crossNode(one).v);
        System.out.printf("环长度=%-2d\n", loopLength(one));

        /**one结环**/
        ten.next = one;
        System.out.printf("ten.next=%2d, 有环?%b\t", 1, isLooped(one));
        System.out.printf("入环节点=%s\t", crossNode(one).v);
        System.out.printf("环长度=%-2d\n", loopLength(one));

        ten.next = seven;
        System.out.printf("ten.next=%2d, 有环?%b\t", 7, isLooped(one));
        System.out.printf("入环节点=%s\t", crossNode(one).v);
        System.out.printf("环长度=%-2d\n", loopLength(one));


        ten.next = eight;
        System.out.printf("ten.next=%2d, 有环?%b\t", 8, isLooped(one));
        System.out.printf("入环节点=%s\t", crossNode(one).v);
        System.out.printf("环长度=%-2d\n", loopLength(one));


        ten.next = nine;
        System.out.printf("ten.next=%2d, 有环?%b\t", 9, isLooped(one));
        System.out.printf("入环节点=%s\t", crossNode(one).v);
        System.out.printf("环长度=%-2d\n", loopLength(one));

        ten.next = ten;
        System.out.printf("ten.next=%2d, 有环?%b\t", 10, isLooped(one));
        System.out.printf("入环节点=%s\t", crossNode(one).v);
        System.out.printf("环长度=%-2d\n", loopLength(one));
    }
}
