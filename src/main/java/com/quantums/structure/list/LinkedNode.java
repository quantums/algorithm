package com.quantums.structure.list;

public class LinkedNode<T> {
    T v;
    LinkedNode next;

    public LinkedNode(T t){
        this(t,null);
    }

    public LinkedNode(T t, LinkedNode n) {
        this.v = t;
        this.next = n;
    }

    public LinkedNode() {
        this(null, null);
    }

    @Override
    public String toString() {
        return "Node{" +
                "v=" + v +
                ", next=" + next.v +
                '}';
    }


    public String toListString() {
        LinkedNode node = this;
        StringBuffer buffer = new StringBuffer("[");
        while (node != null) {
            buffer.append("v=" + node.v + " ");
            node = node.next;
        }
        buffer.delete(buffer.length()-1,buffer.length());
        buffer.append("]");
        return buffer.toString();
    }
}
